// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Tank.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"



// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();

    Tank = Cast<ATank>(UGameplayStatics::GetPlayerPawn(this, 0));

    GetWorldTimerManager().SetTimer(
        FireRateTimerHandle, 
        this, 
        &ATower::CheckFireCondition, 
        FireRate,
        true
    );
	
}

bool ATower::IsInFireRange(){
    if(Tank && Tank->bAlive){
        // Find the distance to the tank
        float Distance = FVector::Dist(GetActorLocation(), Tank->GetActorLocation());
        return Distance <= FireRange;
    }else{
        return false;
    }    
}

void ATower::CheckFireCondition(){
    if(IsInFireRange()){
        Fire();
    }
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    // is in range?
    if(IsInFireRange()){
        // rotate turret
        RotateTurret(Tank->GetActorLocation());  
    }        
}

void ATower::HandleDestruction(){
    Super::HandleDestruction();
    Destroy();
}