// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

// forward declaration for sounds
class USoundBase;

UCLASS()
class TOONTANKS_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(Category="Components", EditDefaultsOnly)
	UStaticMeshComponent* ProjectileMesh;

	UPROPERTY(Category="Components", VisibleAnywhere)
	class UProjectileMovementComponent* ProjectileMovementComponent;

	UPROPERTY(Category="Components", VisibleAnywhere)
	class UParticleSystemComponent* TrailParticles;

	UPROPERTY(EditAnywhere)
	float Damage{50.f};

	UFUNCTION()
	void OnHit(
		// the one that hits
		UPrimitiveComponent* HitComp,
		// the one that's hit
		AActor* OtherActor,
		// the component that's hit
		UPrimitiveComponent* OtherComp,
		// the impulse for physix
		FVector NormalImpulse,
		// the hit result information
		const FHitResult& Hit
	);

	UPROPERTY(EditAnywhere)
	class UParticleSystem* HitParticles;

	UPROPERTY(EditAnywhere)
	USoundBase* LaunchSound;

	UPROPERTY(EditAnywhere)
	USoundBase* HitSound;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UCameraShakeBase> HitCameraShakeClass;

};
